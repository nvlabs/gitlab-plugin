# NeuVector Vulnerability Scanner for GitLab

## Description
NeuVector delivers the only cloud-native Kubernetes security platform with uncompromising end-to-end protection from DevOps vulnerability protection to automated run-time security, and featuring a true Layer 7 container firewall.  

NeuVector automates security for the entire CI/CD pipeline, from Build to Ship to Run. Use the GitLab scanner plug-in to scan during the build phase. Then use NeuVector to monitor images in registries and run automated tests for security compliance.
NeuVector scans your Docker image (container) for known vulnerabilities by using the NeuVector Scanner.  This scanner allows the configuration of one image to be
scanned, and has the option to enable layered scanning for reporting vulnerabilities in each layer of the image, including the build commands.  

The vulnerability scan criteria thresholds have been preconfigured to "0" by default, which would bypass the criteria evaluation. For non-zero threshold criteria, a non-zero exit
code will be flagged for this scan job when the found vulnerabilities equal or exceed these thresholds. 
 
You can config gitlab variable "allow_failure" to stop the pipeline when the scan job fails to pass the criteria evaluation.

Three artifacts (security-report.csv, scan-repository.json, scan-summary.txt) are ready to download when the scan job gets finished.

## Configuration
To enable NeuVector Scanning, you can include or copy the public accessible [scan.yml](https://gitlab.com/neuvector/gitlab-plugin/-/raw/master/scan.yml) into your .gitlab-ci.yml

This template contains the neuvector_scan job that you can customize it in your .gitlab-ci.yml.

## Use cases

1. to scan a docker image
add NV_REGISTRY_USER, NV_REGISTRY_PASSWORD and LICENSE in gitlab setting -> CI/CD -> Variables
    ```shell script
    include:
     - remote: 'https://gitlab.com/neuvector/gitlab-plugin/-/raw/master/scan.yml'
   
    stages:
     - build_job
     - test_job
   
    build-image_job:
      image: docker:latest
      services:
        - docker:dind
      stage: build_job
      script:
        - docker info
        - docker pull alpine:3.3
        - docker tag alpine:3.3 demo:1.0
        # archive a running container to a tar file for scanning
        - docker save -o temp.tar demo:1.0
      artifacts:
        expire_in: 1 hours
        paths:
          - temp.tar
      
    neuvector_scan:
      stage: test_job
      variables:
        scan_local_image: "true"
        image_tar: "temp.tar"
        image_repo: "demo"
        image_tag: "1.0"
        nv_registry_user: $NV_REGISTRY_USER
        nv_registry_password: $NV_REGISTRY_PASSWORD
        nv_license: $LICENSE
        scan_layers: "false"
        high_vul_to_fail: 9
        medium_vul_to_fail: 10
        vul_names_to_fail: "CVE-2020-1971, CVE-2020-1972"
    ```
2. to scan a private registry
add IMAGE_REGISTRY_USER, IMAGE_REGISTRY_PASSWORD, NV_REGISTRY_USER, NV_REGISTRY_PASSWORD, LICENSE in gitlab setting -> CI/CD -> Variables

    ```shell script
    include:
     - remote: 'https://gitlab.com/neuvector/gitlab-plugin/-/raw/master/scan.yml'
   
    neuvector_scan:
      variables:
        image_registry_url: "https://registry.hub.docker.com"
        image_registry_user: $IMAGE_REGISTRY_USER
        image_registry_password: $IMAGE_REGISTRY_PASSWORD
        image_repo: "alpine"
        image_tag: "3.6"
        nv_registry_user: $NV_REGISTRY_USER
        nv_registry_password: $NV_REGISTRY_PASSWORD
        nv_license: $LICENSE
        scan_layers: "false"
        high_vul_to_fail: 5
        medium_vul_to_fail: 9
        vul_names_to_fail: "CVE-2020-1971, CVE-2020-1972"
    ```

3. to scan a registry using NeuVector Restful API
add IMAGE_REGISTRY_USER, IMAGE_REGISTRY_PASSWORD, NV_REGISTRY_USER, NV_REGISTRY_PASSWORD in gitlab setting -> CI/CD -> Variables
    ```shell script
    include:
     - remote: 'https://gitlab.com/neuvector/gitlab-plugin/-/raw/master/scan.yml'
    
    neuvector_scan:
      variables:
        image_registry_url: "https://10.10.1.123:4567"
        image_registry_user: $IMAGE_REGISTRY_USER
        image_registry_password: $IMAGE_REGISTRY_PASSWORD
        image_repo: "Your registry"
        image_tag: "Your tag"
        nv_service_ip: "138.91.251.89"
        nv_service_port: "443"
        nv_service_login_user: $NV_SERVICE_LOGIN_USER
        nv_service_login_password: $NV_SERVICE_LOGIN_PASSWORD
        scan_layers: "false"
        high_vul_to_fail: 10
        medium_vul_to_fail: 9
        vul_names_to_fail: "CVE-2020-1971, CVE-2020-1972"
    ```

## Notes

All variables of neuvector_scan job

| Variable | Description | Default | Type |
| :--- | :--- | :--- | :--- |
| scan_local_image | the flag to scan a docker image | "false" | String |
| image_tar | the tar file archived from a running container | "" | String |
| image_repo | the to-be-scanned docker image/registry name | "" | String |
| image_tag | the to-be-scanned docker image/registry tag | "" | String |
| image_registry_url | the to-be-scanned registry url | "" | String |
| image_registry_user | the login user of the to-be-scanned registry url | "" | String |
| image_registry_password | the login password of the to-be-scanned registry url | "" | String |
| nv_registry_url | the registry url to pull NeuVector Scanner | "" | String |
| nv_registry_user | the login user of the registry url to pull NeuVector Scanner | "" | String |
| nv_registry_password | the login password of the registry url to pull NeuVector Scanner | "" | String |
| nv_license | NeuVector license | "" | String |
| nv_service_ip | the ip of NeuVector Restful Service | "" | String |
| nv_service_port | the port of NeuVector Restful Service | "443" | String |
| nv_service_login_user | the login user of NeuVector Restful Service | "" | String |
| nv_service_login_password | the login password of NeuVector Restful Service | "" | String |
| scan_layers | the flag to enable the layer scanning | "false" | String |
| high_vul_to_fail | the number of high vulnerabilities to fail the scan | 0 | number |
| medium_vul_to_fail | the number of medium vulnerabilities to fail the scan | 0  | number |
| vul_names_to_fail | comma separated vulnerability name list | "" | String |

## Result:

### 1. Pass the scan criteria

![Pass criteria](images/pass.png)

### 2. Failed to meet the scan criteria

![Fail criteria](images/fail.png)
